# .vim_local

Это репозиторий дополнительных (локальных) плагинов к моему основному [конфигу](https://bitbucket.org/0x255/.vim).

## Общие принципы
Все плагины хранятся в директории bundle-available/ для того чтобы задействовать необходимый плагин нужно создать символическую ссылку на него в директории bundle/

```bash
git clone --recurse-submodules https://bitbucket.org/0x255/.vim_local.git $HOME/.vim_local

cd $HOME/.vim_local/bundle/
ln -s ../bundle-available/<имя плагина> .
```
